package poc.com.br.tratarsimulacao;

import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public abstract class UtilTest {

    public String getStrFromResourceFile(String filename) {
        String content = "";
        try {
            InputStream resourceStream = getResourceStream(filename);
            content = convertStreamToString(resourceStream);
        } catch (Exception e) {
            Log.d(this.getClass().getSimpleName(), e.getMessage(), e);
        }

        return content;
    }

    private String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();
        return sb.toString();
    }

    private InputStream getResourceStream(String fileName) {
        ClassLoader classLoader = this.getClass().getClassLoader();
        return classLoader.getResourceAsStream(fileName);
    }

    protected Response getData(String fileName) {
        String strJson = getStrFromResourceFile(fileName);
        return new Gson().fromJson(strJson, Response.class);
    }
}
